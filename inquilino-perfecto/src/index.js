import React from 'react';
import { createRoot } from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom';
import useLocalStorage from './hooks/useLocalStorage';

const rootElement = document.getElementById('root');
const root = createRoot(rootElement);

export const TokenContext = React.createContext();
const TokenProvider = (props) => {
  const [token, setToken] = useLocalStorage('token');
  return (
    <TokenContext.Provider value={[token, setToken]}>
      {props.children}
    </TokenContext.Provider>
  );
};

export const UserContext = React.createContext();
const UserProvider = (props) => {
  const [idUser, setIdUser] = useLocalStorage('idUser');
  return (
    <UserContext.Provider value={[idUser, setIdUser]}>
      {props.children}
    </UserContext.Provider>
  );
};

root.render(
  <React.StrictMode>
    <TokenProvider>
      <UserProvider>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </UserProvider>
    </TokenProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
