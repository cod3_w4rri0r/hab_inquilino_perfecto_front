import { useEffect, useState } from 'react';

const useLocalStorage = (key) => {
  const [value, setValue] = useState(key ? localStorage.getItem(key) : '');
  useEffect(() => {
    console.log('¡Ejecutando el efecto!');
    localStorage.setItem(key, value);
  }, [key, value]);
  return [value, setValue];
};

export default useLocalStorage;
