import './App.css';
import Main from './components/Main';
import Footer from './components/Footer';
import React from 'react';
import LandingFilter from './components/LandingFilter';

function Home({ props }) {
  console.log('Se ejecutó el return de Home', props);

  return (
    <div>
      {/* <Main /> */}
      <Main>
        <LandingFilter props={props} />
      </Main>
      <Footer />
    </div>
  );
}

export default Home;
