import React, { useContext } from 'react';
import '../css/UserMenu.css';
import { TokenContext } from '..';
import { useNavigate } from 'react-router-dom';

function UserMenu({ setLogged, setVisibility, role }) {
  const [token] = useContext(TokenContext);
  const navigate = useNavigate();
  let options;

  const handleClick = (e) => {
    e.stopPropagation();
    console.log(e.target.textContent);
    switch (e.target.textContent) {
      case 'Mi perfil':
        setVisibility(false);
        navigate('/profile');
        break;

      case 'Mis alquileres':
        setVisibility(false);
        navigate('/my-rentals');
        break;

      case 'Mis inmuebles':
        setVisibility(false);
        navigate('/my-estates');
        break;

      case 'Mis valoraciones':
        setVisibility(false);
        navigate('/my-ratings');
        break;

      default:
        break;
    }
  };

  const logout = async (e) => {
    e.stopPropagation();
    const urlLogout = 'http://localhost:4000/users/logout';
    try {
      const res = await fetch(urlLogout, {
        method: 'PUT',
        headers: {
          Authorization: token,
        },
      });
      if (res.ok) {
        setLogged(false);
        localStorage.setItem('token', 'null');
        localStorage.setItem('idUser', 'null');
        navigate('/');
      }
    } catch (error) {
      console.error(error);
    }
  };

  switch (role) {
    case 'renter':
      options = ['Mi perfil', 'Mis alquileres', 'Mis valoraciones'];
      break;

    case 'landlord':
      options = ['Mi perfil', 'Mis inmuebles', 'Mis valoraciones'];
      break;

    case 'hybrid':
      options = [
        'Mi perfil',
        'Mis alquileres',
        'Mis inmuebles',
        'Mis valoraciones',
      ];
      break;

    default:
      console.log('Something really bad happened!');
      break;
  }

  return (
    <div className='user-menu-container'>
      <div className='user-menu'>
        <menu>
          {options.map((option) => (
            <li key={option} onClick={handleClick}>
              {option}
            </li>
          ))}
          <li key='logout' onClick={logout}>
            Cerrar sesión
          </li>
        </menu>
      </div>
    </div>
  );
}

export default UserMenu;
