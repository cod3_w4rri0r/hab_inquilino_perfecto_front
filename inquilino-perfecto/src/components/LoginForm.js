import React, { useContext, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { TokenContext, UserContext } from '..';
import '../css/LoginForm.css';

const LoginForm = ({ url, setLogged, setShow }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  const [, setToken] = useContext(TokenContext);
  const [, setIdUser] = useContext(UserContext);
  const navigate = useNavigate();

  const handleEmail = (e) => {
    console.log('Email:', e.target.value);
    e.preventDefault();
    setEmail(e.target.value);
  };

  const handlePassword = (e) => {
    e.preventDefault();
    console.log('Password: ', e.target.value);
    setPassword(e.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const reqBody = {
      email: email,
      password: password,
    };
    const res = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(reqBody),
    });
    const { data } = await res.json();
    console.log('data', data);
    if (res.ok) {
      setToken(data.token);
      setIdUser(data.id);
      setLogged(true);
      navigate('/');
      setShow(false);
    } else {
      setError(error);
      console.log(error);
    }
  };

  console.log('url', url);

  return (
    <form className='login-form' onSubmit={handleSubmit}>
      <label htmlFor='email'>
        Email: <input id='email' value={email} onChange={handleEmail}></input>
      </label>
      <label>
        Contraseña:{' '}
        <input
          id='password'
          type='password'
          value={password}
          onChange={handlePassword}
        />
      </label>
      <Link to='/passwordRecovery'>Olvidé mi contraseña</Link>
      <button>Iniciar sesión</button>
      {error && <div className='error'>{error}</div>}
    </form>
  );
};

export default LoginForm;
