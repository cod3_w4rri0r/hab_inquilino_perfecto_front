import React, { useState } from 'react';
import '../css/UserAvatar.css';
import UserMenu from './UserMenu';

const UserAvatar = ({ name, avatarUrl, setLogged, role }) => {
  const serverUrl = avatarUrl
    ? 'http://localhost:4000/uploads/' + avatarUrl
    : './logo192.png';
  const [visibility, setVisibility] = useState(false);

  const handleClick = (e) => {
    e.stopPropagation();
    console.log('¡Click!');
    setVisibility(!visibility);
  };

  return (
    <div className='user-avatar' onClick={handleClick}>
      <img src={serverUrl} alt='User avatar' />
      <span>{name}</span>
      {visibility && (
        <UserMenu
          setLogged={setLogged}
          setVisibility={setVisibility}
          role={role}
        />
      )}
    </div>
  );
};

export default UserAvatar;
