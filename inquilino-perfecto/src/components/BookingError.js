import React from 'react';
import { useNavigate } from 'react-router-dom';
import '../css/BookingError.css';

function BookingError({ props }) {
  console.log('props en BookingError:', props);
  const navigate = useNavigate();
  const isLogged =
    props === 'Error: Para poder reservar un inmueble debes iniciar sesión.'
      ? false
      : true; //false;
  console.log('isLogged: ', isLogged);

  const handleClick = (e) => {
    e.preventDefault();
    e.stopPropagation();
    navigate(-3);
  };

  return (
    <div id='booking-error-container'>
      <h1 id='booking-error-message'>{props}</h1>
      <div id='buttons-area'>
        {!isLogged && (
          <>
            <button
              className='booking-error-button'
              onClick={() => navigate('/login')}
            >
              Iniciar sesión
            </button>
            <button
              className='booking-error-button'
              onClick={() => navigate('/register')}
            >
              Regístrate
            </button>
          </>
        )}
        <button className='booking-error-button' onClick={handleClick}>
          Volver a inmuebles
        </button>
      </div>
    </div>
  );
}

export default BookingError;
