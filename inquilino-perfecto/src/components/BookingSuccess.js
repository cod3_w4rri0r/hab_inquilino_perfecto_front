import React from 'react';
import { useNavigate } from 'react-router-dom';
import '../css/BookingSuccess.css';

function BookingSuccess(props) {
  const navigate = useNavigate();
  const handleHomeClick = (e) => {
    e.preventDefault();
    e.stopPropagation();
    navigate('/');
  };
  const handleEstateClick = (e) => {
    e.preventDefault();
    e.stopPropagation();
    navigate(-2);
  };

  return (
    <div id='booking-success-container'>
      <h1 id='booking-success-title'>¡Tu reserva se ha realizado con éxito</h1>
      <h3 id='booking-success-subtitle'>
        El propietario del inmueble tiene que resolver tu solicitud. Te
        notificaremos tan pronto se produzca la resolución. ¡Gracias por usar
        Inquilino perfecto!
      </h3>
      <div id='success-buttons'>
        <button id='success-to-home' onClick={handleHomeClick}>
          Volver a inicio
        </button>
        <button id='success-to-estate' onClick={handleEstateClick}>
          Volver al inmueble
        </button>
      </div>
    </div>
  );
}

export default BookingSuccess;
