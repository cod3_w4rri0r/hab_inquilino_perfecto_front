import React from 'react';
import '../css/ResultsList.css';
import ResultElement from './ResultElement';

function ResultsList({ props }) {
  console.log('props', props);

  return (
    <div className='searching-results-container'>
      <h1>Esta es la lista de resultados de búsqueda</h1>
      <span>Hay {props.length} resultados de búsqueda</span>
      <ul className='results-list'>
        {props.map((result) => (
          <li key={result.id} id={result.id}>
            <ResultElement props={result} />
          </li>
        ))}
      </ul>
    </div>
  );
}

export default ResultsList;
