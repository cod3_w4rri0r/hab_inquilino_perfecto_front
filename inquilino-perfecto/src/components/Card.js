import React from 'react';
import '../css/Card.css';

const Card = ({ type = 'card', colour, radius, shadow, children }) => {
  return (
    <div
      className={type}
      style={{
        backgroundColor: colour,
        borderRadius: radius,
        boxShadow: shadow,
      }}
    >
      {children}
    </div>
  );
};

export default Card;
