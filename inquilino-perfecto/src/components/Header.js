import '../css/Header.css';
import favicon from '../logo.svg';
import React, { useContext, useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { TokenContext, UserContext } from '..';
import UserAvatar from './UserAvatar';

function Header({ url, logged, setLogged, setShow }) {
  const [user, setUser] = useState('');
  const [token] = useContext(TokenContext);
  const [idUser] = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    const getUser = async () => {
      if (token !== 'null' && idUser !== 'null') {
        try {
          const res = await fetch(url + '/users/' + idUser, {
            headers: {
              Authorization: token,
            },
          });
          const { data } = await res.json();
          setUser(data.user);
        } catch (error) {
          console.error(error);
        }
      }
    };
    getUser();
  }, [token]);

  return (
    <header>
      <img
        className='logo'
        src={favicon}
        alt='logo'
        onClick={() => navigate('/')}
      />
      <h1>Inquilino Perfecto</h1>
      {!logged ? (
        <>
          <Link to='/login' onClick={() => setShow(true)}>
            Inicia Sesión
          </Link>
          <Link to='/register'>Regístrate</Link>
        </>
      ) : (
        <UserAvatar
          name={user.name}
          avatarUrl={user.avatar}
          role={user.role}
          setLogged={setLogged}
        />
      )}
    </header>
  );
}

export default Header;
