import React from 'react';
import '../css/Register.css';
import Card from './Card';
import RegisterForm from './RegisterForm';

function Register(props) {
  const { url } = props;
  return (
    <Card>
      <RegisterForm url={url} />
    </Card>
  );
}

export default Register;
