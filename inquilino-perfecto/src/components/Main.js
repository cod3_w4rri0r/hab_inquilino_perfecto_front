import React from 'react';
import '../css/Main.css';
// import LandingFilter from './LandingFilter';

function Main(props) {
  return (
    <main>
      <h1>Este es el Main.</h1>
      {/* <LandingFilter url='http://localhost:4000/estates/search' /> */}
      {props.children}
    </main>
  );
}

export default Main;
