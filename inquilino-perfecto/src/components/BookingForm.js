import React, { useContext } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { TokenContext } from '..';
import '../css/BookingForm.css';

function BookingForm({ props }) {
  const [token] = useContext(TokenContext);
  console.log('props en BookingForm: ', props);
  const { idEstate } = useParams();
  const [, , setErrorMessage] = props;
  const navigate = useNavigate();

  let currentPlaceholder = 'Escribe tu mensaje aquí...';

  const reqBody = {
    idEstate,
  };

  // const textAreaOnFocus = (e) => {
  //   e.preventDefault();
  //   e.stopPropagation();
  //   console.log('onFocus');
  //   currentPlaceholder = '';
  // };

  const handleSubmit = async (e) => {
    e.preventDefault();
    e.stopPropagation();

    try {
      const res = await fetch(props[1] + '/rentals/newRequest', {
        method: 'POST',
        headers: {
          'Content-type': 'application/json',
          authorization: token,
        },
        body: JSON.stringify(reqBody),
      });
      const data = await res.json();
      console.log('res: ', res);
      console.log('data: ', data);
      console.log('token TokenContext: ', token);

      //if (data.status === 'error') {
      if (res.ok) {
        navigate('/estate/:idEstate/booking/success');
      } else {
        switch (data.message) {
          case 'The token is not valid':
            setErrorMessage(
              'Error: Para poder reservar un inmueble debes iniciar sesión.'
            );
            break;
          case 'ERROR: Unavailable estate':
            setErrorMessage(
              'Error: Este inmueble no se encuentra disponible en estos momentos.'
            );
            break;
          case 'ERROR: Invalid request':
            setErrorMessage(
              'Error: Un propietario no puede reservar ningún inmueble de su propiedad.' +
                ' Seleccione otro inmueble y vuelva a intentarlo.'
            );
            break;
          case 'Request still pending for this estate':
            setErrorMessage(
              'Error: Ya ha realizado una solicitud de alquiler para este inmueble.' +
                ' Por favor, espere a que su propietario resuelva su solicitud de alquiler antes de' +
                ' volver a realizar una solicitud nueva.'
            );
            break;
          case 'ERROR: Too short time between following requests for this estate':
            setErrorMessage(
              'Error: Su última solicitud de alquiler para este inmueble' +
                ' se resolvió negativamente hace poco. En estos casos, debe esperar al menos' +
                ' una semana para volver a solicitar alquilar el mismo inmueble.'
            );
            break;
          case 'ERROR: Rental pending on a previous request':
            setErrorMessage(
              'Error: Su última solicitud de alquiler para este inmueble' +
                ' ya ha sido realizada y aceptada, el propietario del inmueble se pondrá en' +
                ' contacto con usted en breve.'
            );
            break;
          default:
            setErrorMessage(
              'Error: Ocurrió un error inesperado. Por favor, vuelva a' +
                ' intentarlo más tarde. Disculpe las molestias.'
            );
            break;
        }
        navigate('/estate/:idEstate/booking/error');
      }
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div className='booking-form-container'>
      <h1>Esto es un formulario de reserva</h1>
      <form className='booking-form' onSubmit={handleSubmit}>
        <label htmlFor='comment'> Mensaje (opcional):</label>
        <textarea
          id='comment'
          className='optional-message'
          placeholder={currentPlaceholder}
        ></textarea>
        <button className='cancel'>Cancelar</button>
        <button className='accept' type='submit'>
          Enviar
        </button>
      </form>
    </div>
  );
}

export default BookingForm;
