import React from 'react';
import { Link } from 'react-router-dom';
import '../css/Footer.css';
import About from './About';
import Contact from '../pages/Contact';
import PrivacyPolicy from './PrivacyPolicy';
import Terms from './Terms';

function Footer() {
  return (
    <footer>
      <Link to='about' element={<About />}>
        Sobre Nosotros
      </Link>
      <Link to='terms' element={<Terms />}>
        Términos y Condiciones
      </Link>
      <Link to='privacy' element={<PrivacyPolicy />}>
        Política de privacidad
      </Link>
      <Link to='contact' element={<Contact />}>
        Contacto
      </Link>
    </footer>
  );
}

export default Footer;
