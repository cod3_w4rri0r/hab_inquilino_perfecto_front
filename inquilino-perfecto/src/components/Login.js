import React from 'react';
import '../css/Login.css';
import Card from './Card';
import LoginForm from './LoginForm';
import Modal from './Modal';

function Login({ url, setLogged, show, setShow }) {
  return (
    <div className='login-container'>
      <h1>Login page</h1>
      <Modal show={show} setShow={setShow}>
        <Card
          colour='white'
          radius={10}
          shadow={'2px 2px 5px grey'}
          type='card'
        >
          <LoginForm url={url} setLogged={setLogged} setShow={setShow} />
        </Card>
      </Modal>
    </div>
  );
}

export default Login;
