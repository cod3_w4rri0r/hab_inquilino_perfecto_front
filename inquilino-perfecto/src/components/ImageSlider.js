import React from 'react';
import '../css/ImageSlider.css';
import favicon from '../logo.svg';

function ImageSlider({ props }) {
  // Por props deben llegar las imágenes
  console.log('props ImageSlider: ', props);
  console.log('props[1].photos[0].name: ', props.photos[0].name);
  const imgNumber = props.photos.length;
  let currentImg = 1;
  const imgUrl = props.photos[0].name
    ? 'http://localhost:4000/uploads/' + props.photos[0].name
    : favicon;
  return (
    <div className='slider-container'>
      {/* <h1>Esto es un Slider de imágenes</h1> */}
      <div className='slider'>
        {/*</div>*/}
        {/* <div className='slide-next'></div>
      <div className='slide-previous'></div> */}
        {/* <div className='mySlides fade'> */}
        <div className='numbertext'>
          {currentImg} / {imgNumber}
        </div>
        {/* <img className='slider-image' src={imgUrl}></img> */}
        <ul className='image-list'>
          {props.photos.map((image) => (
            <li className='slider-image' key={image.id} id={image.id}>
              <img src={'http://localhost:4000/uploads/' + image.name}></img>
            </li>
          ))}
        </ul>
        {/* <div className='text'>Caption Text</div> */}
      </div>
      <div className='img-index'></div>
      <a className='prev'>&#10094;</a>
      <a className='next'>&#10095;</a>
    </div>
  );
}

export default ImageSlider;
