import React from 'react';
import { useNavigate } from 'react-router-dom';
import '../css/ResultElement.css';

function ResultElement({ props }) {
  console.log('props_RE:', props);
  const navigate = useNavigate();

  const handleClick = (e) => {
    e.preventDefault();
    e.stopPropagation();

    console.log('Clické en ', e.target.id);
    navigate(`/estate/${e.target.id}`);
  };

  return (
    <div
      id={props.id}
      className='result-element-container'
      onClick={handleClick}
    >
      <div className='result-image-slider'>
        <img src='./logo192.png' alt='result' />
      </div>
      <div className='result-info'>
        <span>Localidad: {props.location} </span>
        <span>Precio: {props.price} </span>
      </div>
    </div>
  );
}

export default ResultElement;
