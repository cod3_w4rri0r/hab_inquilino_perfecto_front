import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import '../css/EstateDetail.css';
import ImageSlider from './ImageSlider';

function EstateDetail({ props }) {
  const params = useParams();
  const navigate = useNavigate();
  console.log('params:', params);
  console.log('props:', props);

  let item = props.find((x) => x.id === parseInt(params.idEstate));

  console.log('item:', item);

  const handleBookingClick = (e) => {
    e.preventDefault(); // No sé si hace falta o es sólo con los enlaces
    navigate(`/estate/${params.idEstate}/booking`);
  };

  return (
    <div className='main-container'>
      <h1>Esto es el detalle de un inmueble, el {params.idEstate}</h1>
      {/* <div className='slider-container'>SLIDER</div> */}
      <ImageSlider props={item} />
      <div className='ratings-container'>
        SCORE: {item.score != null ? item.score : '0.0'}
      </div>
      <div className='estate-details-container'>
        DETAILS
        <div>
          <span>Localidad: </span>
          {item.location}
        </div>
        <div>
          <span>Precio: </span>
          {item.price}
        </div>
        <div>
          <span>Extras: </span>
          {item.balcony > 0 && 'Dispone de balcón'}
        </div>
      </div>
      <div className='description'>DESCRIPTION</div>
      <button className='button1'>Button 1</button>
      <button className='reservar' onClick={handleBookingClick}>
        RESERVAR
      </button>
      <div className='comments'>RESEÑAS</div>
      {/* </h1> */}
    </div>
  );
}

export default EstateDetail;
