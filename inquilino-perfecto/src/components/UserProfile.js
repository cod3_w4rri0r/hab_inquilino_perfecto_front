import React, { useContext, useEffect, useState } from 'react';
import { TokenContext, UserContext } from '..';
import '../css/UserProfile.css';

function UserProfile() {
  const [idUser] = useContext(UserContext);
  const [token] = useContext(TokenContext);

  const [name, setName] = useState('');

  const handleClick = (e) => {
    e.preventDefault();
  };

  useEffect(() => {
    const getUseData = async () => {
      try {
        const res = await fetch(`http://localhost:4000/users/${idUser}`, {
          headers: { authorization: token },
        });
        const userData = await res.json();
        setName(userData.data.user.name);
        console.log('UserData: ', userData);
      } catch (error) {}
    };

    getUseData();
  }, []);

  return (
    <div className='container'>
      <aside className='aside'>Este es el aside</aside>
      <div className='datos'>
        <ul className='datosusuario'>
          <li>Nombre: {name} </li>
          <li>Email</li>
          <li>Telefono</li>
          <li>Localidad</li>
        </ul>
        <p className='bio'>Esta es la BIO</p>
        <button onClick={handleClick} className='boton'>
          Aceptar/Modificar
        </button>
      </div>
    </div>
  );
}

export default UserProfile;
