import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import '../css/LandingFilter.css';

//function LandingFilter({ url }) {
function LandingFilter({ props }) {
  const [locationsList, setLocationsList] = useState([]);
  const [search, setSearch] = useState('');
  const [price, setPrice] = useState('');
  const [numRooms, setNumRooms] = useState('');
  const [dataAvail, setDateAvail] = useState('');
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const navigate = useNavigate();

  const { apiURL } = props;
  const { setEstates } = props;

  const reqBody = {
    location: search,
    minPrice,
    maxPrice,
    minNumBedrooms: numRooms,
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const defUrl = new URL(props.apiURL + '/estates/search');
    for (let k in reqBody) {
      if (reqBody[k] === null) {
        continue;
      } else {
        defUrl.searchParams.append(k, reqBody[k]);
      }
    }
    try {
      const res = await fetch(defUrl);
      const { data } = await res.json();
      console.log('data:', data);
      props.setEstates(data);
      navigate('/results');
    } catch (error) {
      console.error(error);
    }
  };

  const handleClick = (e) => {
    e.preventDefault();
    setSearch(e.target.textContent);
  };

  useEffect(() => {
    const getLocations = async () => {
      const res = await fetch('http://localhost:4000/misc/locations');
      const data = await res.json();
      setLocationsList(data.locationsList);
    };
    getLocations();
  }, []);

  const searchResults =
    search.length > 1
      ? locationsList.filter((city) =>
          city.toLowerCase().includes(search.toLocaleLowerCase())
        )
      : [];

  const handlePrice = (e) => {
    e.preventDefault();
    setPrice(e.target.value);
    switch (e.target.value) {
      case '0':
        setMinMaxPrice(0, 100);
        break;
      case '1':
        setMinMaxPrice(100, 250);
        break;
      case '2':
        setMinMaxPrice(250, 450);
        break;
      case '3':
        setMinMaxPrice(450, 600);
        break;
      case '4':
        setMinMaxPrice(600, 850);
        break;
      case '5':
        setMinMaxPrice(850, null);
        break;
      default:
        setMinPrice(null);
        setMaxPrice(null);
    }
  };

  const setMinMaxPrice = (minPrice, maxPrice) => {
    setMinPrice(minPrice);
    setMaxPrice(maxPrice);
  };

  //console.log('url', props.props.props.apiURL);
  console.log('Lf:', props);

  return (
    <div className='landing-filter-container'>
      <form className='landing-filter' onSubmit={handleSubmit}>
        <section className='matching-items'>
          <label>
            <input
              id='location'
              className='landing-filter-field'
              type='search'
              placeholder='Localidad'
              value={search}
              onChange={(e) => setSearch(e.target.value)}
            ></input>
          </label>
          {searchResults.length ? (
            <ul>
              {searchResults.map((result) => (
                <li key={result} onClick={handleClick}>
                  {result}
                </li>
              ))}
            </ul>
          ) : null}
        </section>
        <select
          name='price'
          id='price-select'
          className='landing-filter-field'
          value={price}
          onChange={handlePrice}
        >
          <option value=''>--Choose a price range--</option>
          <option value='0'>Menos de 100€</option>
          <option value='1'>100€ - 250€</option>
          <option value='2'>250€ - 450€</option>
          <option value='3'>450€ - 600€</option>
          <option value='4'>600€ - 850€</option>
          <option value='5'>850€ o más</option>
        </select>
        <label>
          <input
            id='num-rooms'
            type='number'
            min='1'
            className='landing-filter-field'
            placeholder='Número de habitaciones'
            value={numRooms}
            onChange={(e) => setNumRooms(e.target.value)}
          ></input>
        </label>
        <label>
          <input
            id='availab-date'
            type='date'
            className='landing-filter-field'
            placeholder='Fecha de disponibilidad'
            value={dataAvail}
            onChange={(e) => setDateAvail(e.target.value)}
          ></input>
        </label>
        <button className='landing-submit'>Buscar</button>
      </form>
    </div>
  );
}

export default LandingFilter;
