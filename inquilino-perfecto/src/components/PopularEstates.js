import React from 'react';
import '../css/PopularEstates.css';
import Card from './Card';

const PopularEstates = ({ props }) => {
  const { container } = props;
  const { cardType } = props;
  console.log('container: ', container);
  console.log('cardType:', cardType);
  return (
    <Card colour='white' radius={10} type={container}>
      <Card
        colour='white'
        radius={10}
        shadow={'2px 2px 5px grey'}
        type={cardType}
      >
        <img src='./logo192.png' alt='Estate' />
        <span>Inmueble 1</span>
      </Card>
      <Card
        colour='white'
        radius={10}
        shadow={'2px 2px 5px grey'}
        type={cardType}
      >
        <img src='./logo192.png' alt='Estate' />
        <span>Inmueble 1</span>
      </Card>
      <Card
        colour='white'
        radius={10}
        shadow={'2px 2px 5px grey'}
        type={cardType}
      >
        <img src='./logo192.png' alt='Estate' />
        <span>Inmueble 1</span>
      </Card>
      <Card
        colour='white'
        radius={10}
        shadow={'2px 2px 5px grey'}
        type={cardType}
      >
        <img src='./logo192.png' alt='Estate' />
        <span>Inmueble 1</span>
      </Card>
      <Card
        colour='white'
        radius={10}
        shadow={'2px 2px 5px grey'}
        type={cardType}
      >
        <img src='./logo192.png' alt='Estate' />
        <span>Inmueble 1</span>
      </Card>
      <Card
        colour='white'
        radius={10}
        shadow={'2px 2px 5px grey'}
        type={cardType}
      >
        <img src='./logo192.png' alt='Estate' />
        <span>Inmueble 1</span>
      </Card>
    </Card>
  );
};

export default PopularEstates;
