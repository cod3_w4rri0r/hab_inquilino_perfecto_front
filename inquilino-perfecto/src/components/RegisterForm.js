import React, { useState } from 'react';
//import { AiOutlineEyeInvisible, AiOutlineEye } from 'react-icons/ai';

function RegisterForm({ url }) {
  const [username, setUsername] = useState('');
  const [firstSurname, setFirstSurname] = useState('');
  const [secondSurname, setSecondSurname] = useState('');
  const [email, setEmail] = useState('');
  const [dni, setDni] = useState('');
  const [password, setPassword] = useState('');
  const [password2, setPassword2] = useState('');
  const [bio, setBio] = useState('');

  const [vis, setVis] = useState(false);

  let passwordClassName = vis
    ? 'register-form-field error-field'
    : 'register-form-field';

  const handlePassword = (e) => {
    e.preventDefault();
    setVis(false);
    passwordClassName = 'register-form-field';
    e.target.id === 'password'
      ? setPassword(e.target.value)
      : setPassword2(e.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const surname = secondSurname
      ? firstSurname + ' ' + secondSurname
      : firstSurname;

    setVis(password === password2 ? false : true);

    const reqBody = {
      email,
      password,
      name: username,
      surname,
      dni_nie: dni,
      bio,
    };
    console.log('reqBody', reqBody);
    try {
      await fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(reqBody),
      });
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className='register-container'>
      <h1>Registry page</h1>

      <form className='register-form' onSubmit={handleSubmit}>
        <label>
          Nombre* :
          <input
            id='name'
            className='register-form-field'
            type='text'
            required
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          ></input>
        </label>
        <label>
          Primer apellido* :
          <input
            id='first-surname'
            className='register-form-field'
            type='text'
            required
            value={firstSurname}
            onChange={(e) => setFirstSurname(e.target.value)}
          ></input>
        </label>
        <label>
          Segundo apellido :
          <input
            id='second-surname'
            className='register-form-field'
            type='text'
            value={secondSurname}
            onChange={(e) => setSecondSurname(e.target.value)}
          ></input>
        </label>
        <label>
          DNI* :
          <input
            id='dni'
            className='register-form-field'
            type='text'
            required
            value={dni}
            onChange={(e) => setDni(e.target.value)}
          ></input>
        </label>
        <label>
          Email* :
          <input
            id='email'
            className='register-form-field'
            type='email'
            required
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          ></input>
        </label>
        <label>
          Contraseña* :
          <input
            id='password'
            className={passwordClassName}
            type='password'
            required
            value={password}
            onChange={(e) => handlePassword(e)}
          ></input>
          {/* <AiOutlineEye onClick={handleIcon} /> */}
        </label>
        <label>
          Repite la contraseña* :
          <input
            id='password2'
            className={passwordClassName}
            type='password'
            required
            value={password2}
            onChange={(e) => handlePassword(e)}
          ></input>
        </label>
        {vis ? (
          <span className='error-message'>
            Los campos de contraseña no coinciden
          </span>
        ) : null}
        <label>
          Bio :
          <textarea
            id='bio'
            className='register-form-textarea'
            value={bio}
            onChange={(e) => setBio(e.target.value)}
          ></textarea>
        </label>
        <button>Registrar</button>
      </form>
    </div>
  );
}

export default RegisterForm;
