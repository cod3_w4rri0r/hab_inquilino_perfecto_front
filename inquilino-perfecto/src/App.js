import './App.css';
import React, { useState } from 'react';
import Header from './components/Header';
import Home from './Home';
import Login from './components/Login';
import About from './components/About';
import Terms from './components/Terms';
import PrivacyPolicy from './components/PrivacyPolicy';
import Contact from './pages/Contact';
import { Route, Routes } from 'react-router-dom';
import Register from './components/Register';
import PassRecovery from './components/PassRecovery';
import UserProfile from './components/UserProfile';
import UserRentals from './components/UserRentals';
import UserRatings from './components/UserRatings';
import UserEstates from './components/UserEstates';
import ResultsList from './components/ResultsList';
import EstateDetail from './components/EstateDetail';
import BookingForm from './components/BookingForm';
import BookingSuccess from './components/BookingSuccess';
import BookingError from './components/BookingError';

function App() {
  console.log('Se ejecutó el return de App');
  const apiURL = 'http://localhost:4000';
  const [logged, setLogged] = useState(false);
  const [show, setShow] = useState(false);

  const [estates, setEstates] = useState([]);
  const [errorMessage, setErrorMessage] = useState('');

  return (
    <div className='app-container'>
      <Header
        url={apiURL}
        logged={logged}
        setLogged={setLogged}
        setShow={setShow}
      />

      <Routes>
        <Route
          path='/'
          element={<Home props={{ estates, setEstates, apiURL }} />}
        />
        <Route
          path='/login'
          element={
            <Login
              url={apiURL + '/users/login'}
              setLogged={setLogged}
              show={show}
              setShow={setShow}
            />
          }
        ></Route>
        <Route
          path='/register'
          element={<Register url={apiURL + '/users'} />}
        />
        <Route path='/about' element={<About />} />
        <Route path='/terms' element={<Terms />} />
        <Route path='/privacy' element={<PrivacyPolicy />} />
        <Route path='/contact' element={<Contact />} />
        <Route path='/passwordRecovery' element={<PassRecovery />} />

        <Route path='/profile' element={<UserProfile url={apiURL} />} />
        <Route path='/my-rentals' element={<UserRentals />} />
        <Route path='/my-estates' element={<UserEstates />} />
        <Route path='/my-ratings' element={<UserRatings />} />

        <Route path='/results' element={<ResultsList props={estates} />} />

        <Route
          path='/estate/:idEstate'
          element={<EstateDetail props={estates} />}
        />

        <Route
          path='/estate/:idEstate/booking'
          element={<BookingForm props={[estates, apiURL, setErrorMessage]} />}
        />

        <Route
          path='/estate/:idEstate/booking/success'
          element={<BookingSuccess />}
        />

        <Route
          path='/estate/:idEstate/booking/error'
          element={<BookingError props={errorMessage} />}
        />
      </Routes>
    </div>
  );
}

export default App;
